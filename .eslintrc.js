module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: ["airbnb", "prettier", "prettier/react"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly"
  },
  parser: "babel-eslint",
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018,
    sourceType: "module"
  },
  plugins: ["react"],
  rules: {
    "react/prefer-stateless-function": [0],
    "react/jsx-filename-extension": [0],
    "react/prop-types": [0],
    "jsx-a11y/anchor-has-content": [0],
    "jsx-a11y/anchor-is-valid": [0],
    "no-plusplus": ["error", { allowForLoopAfterthoughts: true }]
  }
};
