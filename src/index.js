import "bootstrap/dist/css/bootstrap.css";
import React from "react";
import ReactDOM from "react-dom";
import { Route } from "react-router-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import { store, history } from "./redux/store";
import "./css/index.css";
import * as serviceWorker from "./serviceWorker";
import "./css/App.css";
import "../node_modules/font-awesome/css/font-awesome.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "jquery/dist/jquery.min";
import "bootstrap/dist/js/bootstrap.min";
import GlobalErrorSwitch from "./Components/GlobalErrorSwitch";

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Route component={GlobalErrorSwitch} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();
