import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import "../css/ItemDetails.css";
import { getOneEntity } from "../redux/actions/adminActions";
import { addToCart } from "../redux/actions/cartActions";

class ItemDetails extends Component {
  componentWillMount() {
    const { match, dispatch } = this.props;
    dispatch(getOneEntity(match.params.id));
  }

  handleAddToCartClick = () => {
    const { match, dispatch } = this.props;
    dispatch(addToCart(match.params.id));
  };

  render() {
    const { item } = this.props;
    return (
      <React.Fragment>
        <div className="container caaa">
          <div className="row">
            <div className="col-md-5">
              <div
                id="carouselExampleControls"
                className="carousel slide"
                data-ride="carousel"
              >
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <img
                      className="d-block w-100"
                      src={item ? item.image1Source : null}
                      alt="First slide"
                    />
                  </div>
                  <div className="carousel-item">
                    <img
                      className="d-block w-100"
                      src={item ? item.image2Source : null}
                      alt="Second slide"
                    />
                  </div>
                  <div className="carousel-item">
                    <img
                      className="d-block w-100"
                      src={item ? item.image1Source : null}
                      alt="Third slide"
                    />
                  </div>
                </div>
                <a
                  className="carousel-control-prev"
                  href="#carouselExampleControls"
                  role="button"
                  data-slide="prev"
                >
                  <span
                    className="carousel-control-prev-icon"
                    aria-hidden="true"
                  />
                  <span className="sr-only">Previous</span>
                </a>
                <a
                  className="carousel-control-next"
                  href="#carouselExampleControls"
                  role="button"
                  data-slide="next"
                >
                  <span
                    className="carousel-control-next-icon"
                    aria-hidden="true"
                  />
                  <span className="sr-only">Next</span>
                </a>
              </div>
            </div>

            <div className="details col-md-7">
              <h2 className="name">{item ? item.productName : null}</h2>
              <hr />
              <h3 className="price-container">${item ? item.price : null}</h3>
              <div className="certified">
                <ul>
                  <li>
                    <a href="#">
                      Delivery time<span>7 Working Days</span>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      Certified<span>Quality Assured</span>
                    </a>
                  </li>
                </ul>
              </div>
              <hr />
              <div className="description description-tabs">
                <ul id="myTab" className="nav nav-pills">
                  <li className="active">
                    <a
                      href="#more-information"
                      data-toggle="tab"
                      className="myadd btn btn-white btn-default no-margin"
                    >
                      Product Description{" "}
                    </a>
                  </li>
                </ul>
                <div id="myTabContent" className="tab-content">
                  <div
                    className="tab-pane fade active in"
                    id="more-information"
                  >
                    <br />
                    <p>{item ? item.information : null}</p>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-12 col-md-6 col-lg-6">
                      <button
                        type="button"
                        className="myadd btn btn-white btn-default"
                        onClick={() => this.handleAddToCartClick()}
                      >
                        <i className="fa fa-shopping-cart" /> Add to cart{" "}
                      </button>
                      <button
                        type="button"
                        className="myadd btn btn-white btn-default"
                      >
                        <i className="fa fa-heart" /> Add to wishlist{" "}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

ItemDetails.propTypes = {
  item: PropTypes.shape.isRequired
};

const mapStateToProps = state => ({
  item: state.admin.oneEntity
});

export default connect(mapStateToProps)(ItemDetails);
