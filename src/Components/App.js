import React, { Component } from "react";
import "../css/App.css";
import { Switch, Route } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";
import Navbar from "./Navbar";
import FooterContainer from "./FooterContainer";
import ItemsGallery from "./ItemsGallery";
import Home from "./Home";
import CartContainer from "./CartContainer";
import NotFound from "./NotFound";
import ItemDetails from "./ItemDetails";
import InfoPage from "./InfoPage";
import { history } from "../redux/store";

const homeItems = [
  { link: "/info_page", imageSource: "first-home.jpg" },
  { link: "/items_female", imageSource: "second-home.jpg" },
  { link: "/items_male", imageSource: "third-home.jpg" }
];

// verifyCredentials(store);

class App extends Component {
  render() {
    return (
      <ConnectedRouter history={history}>
        <Navbar />
        <Switch>
          <Route exact path="/" render={() => <Home items={homeItems} />} />
          <Route path="/item_details/:id" component={ItemDetails} />
          <Route
            path="/items_male"
            component={() => <ItemsGallery gender="male" />}
          />
          <Route
            path="/items_female"
            component={() => <ItemsGallery gender="female" />}
          />
          <Route path="/info_page" component={InfoPage} />
          <Route path="/cart" component={CartContainer} />
          <Route component={NotFound} />
        </Switch>
        <FooterContainer />
      </ConnectedRouter>
    );
  }
}

export default App;
