import React, { Component } from "react";

export default class InfoPage extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="jumbotron text-center">
          <img src="birkin_logo.png" alt="birkin_logo" />
        </div>

        <div className="container">
          <div className="row">
            <div className="col-sm-4">
              <h3>HOW CAN I FIND YOUR INTERNATIONAL DELIVERY INFORMATION?</h3>
              <p>
                You can find all International Delivery information by clicking
                here. Simply select the relevant country in the dropdown and
                you&apos;ll be presented with the different delivery methods
                available and costs. Contents - What are the basics? - How much
                does delivery cost? - Can I track my order? - When will my order
                be delivered? What are the basics? Standard and Express delivery
                services are available for most of the countries that we ship
                to. Once you&apos;ve entered your delivery address, you&apos;ll
                be able to see the available delivery services.*
              </p>
            </div>
            <div className="col-sm-4">
              <h3>HOW DO I KNOW THAT I’M BUYING A GENUINE PRODUCT?</h3>
              <p>
                At Birkin, we only stock genuine and authentic items, bought
                directly from the brands we offer – we don&apos;t buy fakes, so
                you can be sure the item you’ve received is genuine. However, if
                you&apos;re not entirely happy with your item, you’re welcome to
                return it to us for a full refund. All you need to do is
                complete the returns note which was included with your order and
                send it back to us. You can find out more about our Returns
                Policy by clicking here.
              </p>
            </div>
            <div className="col-sm-4">
              <h3>HOW DO I RETURN SOMETHING TO YOU?</h3>
              <p>
                Step 1 You can return your parcel using any postal service. To
                view a list of available returns options, click here and select
                the country you&apos;re returning from. Please note -
                you&apos;ll need to cover the postage and any relevant duty
                costs yourself. It&apos;s a good idea to mark your parcel as
                &apos;returned goods&apos; to avoid being charged any duties.
                Step 2 Pop your returns note in the parcel with your order
                number and which items you&apos;re returning for a refund marked
                on it. Step 3 Remember to ask for proof of postage as the parcel
                remains your responsibility until it arrives with us so if your
                parcel goes missing in the mail, you&apos;ll have proof
                you&apos;ve sent it. Step 4 It can usually take up to 21 working
                days (excluding weekends and public holidays) for your return to
                be delivered back to our warehouse in the UK, depending on your
                location and which postal service you use. Please see the below
                timeframes as a guide: Step 5 We&apos;ll send you an email as
                soon as we&apos;ve received your return back at our warehouse in
                its original condition. Once received, we&apos;ll refund your
                original payment method - this is usually done within 24 hours.
                Step 6 It can then take up to 10 working days for the funds to
                appear back in your account depending on your bank or card
                issuer.
              </p>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
