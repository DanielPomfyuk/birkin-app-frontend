import React, { Component } from "react";
import PropTypes from "prop-types";
import "../css/Home.css";
import HomeImage from "./HomeImage";

class Home extends Component {
  render() {
    const { items } = this.props;
    return (
      <React.Fragment>
        <div className="home-family">
          <ul>
            {items.map(item => (
              <HomeImage {...item} />
            ))}
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

Home.propTypes = {
  items: PropTypes.arrayOf.isRequired
};

export default Home;
