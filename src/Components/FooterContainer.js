import React, { Component } from "react";
import FooterComponent from "./FooterComponent";

class FooterContainer extends Component {
  render() {
    const footerChildren = [
      {
        name: "Contacts",
        links: ["Phone", "Email", "Postcode"]
      },
      {
        name: "About",
        links: ["History", "Partners", "Co-working"]
      },
      {
        name: "Additional",
        links: ["Contest", "Sales", "Something"]
      }
    ];
    const icons = [
      {
        href: "#",
        icoClass: "icoRss",
        title: "Rss",
        className: "fa fa-rss"
      },
      {
        href: "#",
        icoClass: "icoFacebook",
        title: "Facebook",
        className: "fa fa-facebook"
      },
      {
        href: "#",
        icoClass: "icoTwitter",
        title: "Twitter",
        className: "fa fa-twitter"
      },
      {
        href: "#",
        icoClass: "icoGoogle",
        title: "Google +",
        className: "fa fa-google-plus"
      },
      {
        href: "#",
        icoClass: "icoLinkedin",
        title: "Linked in",
        className: "fa fa-linkedin"
      }
    ];
    return <FooterComponent footerChildren={footerChildren} icons={icons} />;
  }
}

export default FooterContainer;
