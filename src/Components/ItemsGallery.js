import React, { Component } from "react";
import { connect } from "react-redux";
import ProductItem from "./ProductItem";
import { getAllEntites } from "../redux/actions/adminActions";

class ItemsGallery extends Component {
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(getAllEntites());
  }

  render() {
    const { gender, items } = this.props;
    const filterdItems = items.filter(item => {
      return item.gender === gender;
    });
    const itemsCollection = filterdItems.map(item => {
      return <ProductItem {...item} />;
    });
    return (
      <div className="container">
        <div className="row">{itemsCollection}</div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  items: state.admin.entities
});

export default connect(mapStateToProps)(ItemsGallery);
