/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-lone-blocks */
import React, { Component } from "react";
import MaterialTable from "material-table";
import { connect } from "react-redux";
import { ExpandMore } from "@material-ui/icons";
import {
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  Dialog,
  Checkbox,
  FormGroup,
  FormControlLabel,
  Grid,
  Paper,
  withStyles,
  makeStyles
} from "@material-ui/core";
import axios from "axios";
import tableIcons from "./tableIcons";
import {
  getAllEntites,
  getOneEntity,
  addEntity,
  editEntity,
  deleteEntity
} from "../../redux/actions/adminActions";
import { API_VERSION_1 } from "../../config";

class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        { title: "ID", field: "id", editable: "never", type: "numeric" },
        { title: "Name", field: "name" },
        { title: "Information", field: "information" },
        { title: "Color", field: "color" },
        { title: "Gender", field: "gender" },
        { title: "Price", field: "price", type: "numeric" },
        { title: "Quantity", field: "quantity", type: "numeric" },
        { title: "Type", field: "sce_type" }
      ],
      open: false,
      currId: 0,
      checkedXS: false,
      checkedS: false,
      checkedM: false,
      checkedL: false,
      checkedXL: false
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(getAllEntites());
  }

  createSizes = sizes => {
    const tmp = [];

    for (let i = 0; i < sizes.length; i++) {
      tmp.push(<p>{sizes[i].size}</p>);
    }

    return tmp;
  };

  handleClickOpen = id => {
    this.setState({
      open: true,
      currId: id
    });
    const { dispatch } = this.props;
    dispatch(getOneEntity(id));
  };

  handleClose = () => {
    this.setState({
      open: false
    });
  };

  handleButton = async event => {
    const { files, id } = event.target;
    const { currId } = this.state;
    const lastChar = id[id.length - 1];

    const data = new FormData();
    data.append("file", files[0]);
    data.append("UPLOADCARE_STORE", 1);
    data.append("UPLOADCARE_PUB_KEY", "1e7481249255263a166c");

    const response = await axios.post(
      "https://upload.uploadcare.com/base/",
      data
    );

    await axios.post(
      `${API_VERSION_1}/admin/sce/add_attachment_${lastChar}`,
      JSON.stringify({ id: currId, file: response.data.file }),
      {
        headers: {
          "Content-Type": "application/json",
          "access-token": localStorage.getItem("access-token"),
          client: localStorage.getItem("client"),
          uid: localStorage.getItem("uid"),
          "token-type": localStorage.getItem("token-type"),
          expiry: localStorage.getItem("expiry")
        }
      }
    );
  };

  handleCheckboxChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };

  handleSetSizes = async () => {
    const {
      checkedXS,
      checkedS,
      checkedM,
      checkedL,
      checkedXL,
      currId
    } = this.state;
    const sizes = [];
    if (checkedXS) sizes.push("XS");
    if (checkedS) sizes.push("S");
    if (checkedM) sizes.push("M");
    if (checkedL) sizes.push("L");
    if (checkedXL) sizes.push("XL");
    await axios.post(
      `${API_VERSION_1}/admin/sce/update_sizes`,
      JSON.stringify({ id: currId, sizes }),
      {
        headers: {
          "Content-Type": "application/json",
          "access-token": localStorage.getItem("access-token"),
          client: localStorage.getItem("client"),
          uid: localStorage.getItem("uid"),
          "token-type": localStorage.getItem("token-type"),
          expiry: localStorage.getItem("expiry")
        }
      }
    );
  };

  render() {
    const { columns, open } = this.state;
    const { entities, oneEntity } = this.props;
    return (
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper
            style={{
              display: "flex",
              overflow: "auto",
              flexDirection: "column"
            }}
          >
            <MaterialTable
              title="Editable Example"
              columns={columns}
              data={entities}
              icons={tableIcons}
              onRowClick={(event, rowData) => this.handleClickOpen(rowData.id)}
              editable={{
                onRowAdd: newData =>
                  new Promise((resolve, reject) => {
                    setTimeout(() => {
                      {
                        const { dispatch } = this.props;
                        const normalData = { ...newData, sizes: [] };
                        dispatch(addEntity(normalData));
                        resolve();
                      }
                      resolve();
                    }, 1000);
                  }),
                onRowUpdate: (newData, oldData) =>
                  new Promise((resolve, reject) => {
                    setTimeout(() => {
                      {
                        const { dispatch } = this.props;
                        const normalData = { ...newData, sizes: [] };
                        dispatch(editEntity(oldData.id, normalData));
                        resolve();
                      }
                      resolve();
                    }, 1000);
                  }),
                onRowDelete: oldData =>
                  new Promise((resolve, reject) => {
                    setTimeout(() => {
                      {
                        const { dispatch } = this.props;
                        dispatch(deleteEntity(oldData.id));
                        resolve();
                      }
                      resolve();
                    }, 1000);
                  })
              }}
            />
            <Dialog
              open={open}
              onClose={this.handleClose}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">
                {oneEntity.name} details
              </DialogTitle>
              <DialogContent>
                <FormGroup row>
                  <div className="d-flex flex-row">
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.checkedXS}
                          onChange={this.handleCheckboxChange("checkedXS")}
                          value="checkedXS"
                        />
                      }
                      label="XS"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.checkedS}
                          onChange={this.handleCheckboxChange("checkedS")}
                          value="checkedS"
                        />
                      }
                      label="S"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.checkedM}
                          onChange={this.handleCheckboxChange("checkedM")}
                          value="checkedM"
                        />
                      }
                      label="M"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.checkedL}
                          onChange={this.handleCheckboxChange("checkedL")}
                          value="checkedL"
                        />
                      }
                      label="L"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.checkedXL}
                          onChange={this.handleCheckboxChange("checkedXL")}
                          value="checkedXL"
                        />
                      }
                      label="XL"
                    />
                  </div>
                  <div className="d-flex flex-row-reverse">
                    <span>
                      <Button
                        variant="outlined"
                        className="mr-2 ml-2"
                        onClick={() => this.handleSetSizes()}
                      >
                        Set sizes
                      </Button>
                    </span>
                  </div>
                </FormGroup>
                <span>
                  <input
                    accept="image/*"
                    style={{ display: "none" }}
                    id="outlined-button-file-1"
                    multiple
                    type="file"
                    onChange={event => this.handleButton(event)}
                  />
                  <label htmlFor="outlined-button-file-1">
                    <Button
                      variant="outlined"
                      component="span"
                      className="mr-2"
                    >
                      Upload 1 image
                    </Button>
                  </label>
                </span>
                <span>
                  <input
                    accept="image/*"
                    style={{ display: "none" }}
                    id="outlined-button-file-2"
                    multiple
                    type="file"
                    onChange={event => this.handleButton(event)}
                  />
                  <label htmlFor="outlined-button-file-2">
                    <Button
                      variant="outlined"
                      component="span"
                      className="mr-2"
                    >
                      Upload 2 image
                    </Button>
                  </label>
                </span>
                <span>
                  <input
                    accept="image/*"
                    style={{ display: "none" }}
                    id="outlined-button-file-3"
                    multiple
                    type="file"
                    onChange={event => this.handleButton(event)}
                  />
                  <label htmlFor="outlined-button-file-3">
                    <Button
                      variant="outlined"
                      component="span"
                      className="mr-2"
                    >
                      Upload 3 image
                    </Button>
                  </label>
                </span>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose} color="primary">
                  Close
                </Button>
              </DialogActions>
            </Dialog>
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

export default connect(state => ({
  entities: state.admin.entities,
  oneEntity: state.admin.oneEntity
}))(Products);
