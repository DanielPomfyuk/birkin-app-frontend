import { ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Drawer from "@material-ui/core/Drawer";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import {
  createMuiTheme,
  makeStyles,
  MuiThemeProvider
} from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import DashboardIcon from "@material-ui/icons/Dashboard";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import React from "react";
import { connect } from "react-redux";
import { Link, Redirect, Route, Switch } from "react-router-dom";
import {
  signOutAdmin,
  verifyAdminCredentials
} from "../../redux/admin-auth-config";
import { store } from "../../redux/store";
import Dashboard from "./Dashboard";
import Products from "./Products";

verifyAdminCredentials(store);

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  toolbar: theme.mixins.toolbar,
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  title: {
    flexGrow: 1
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column"
  },
  fixedHeight: {
    height: 240
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex"
    }
  }
}));

const theme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      main: "#BB86FC"
    },
    secondary: {
      main: "#03DAC5"
    }
  }
});

function AdminApp(props) {
  // eslint-disable-next-line no-shadow
  const { match, history, signOutAdmin } = props;
  const classes = useStyles();

  const signOut = e => {
    e.preventDefault();
    signOutAdmin()
      .then(() => history.push("/"))
      .catch(() => history.push("/not-found"));
  };

  return (
    <MuiThemeProvider theme={theme}>
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="absolute" className={classes.appBar} color="default">
          <Toolbar>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              className={classes.title}
            >
              Admin app
            </Typography>
            <Toolbar>
              <IconButton color="inherit" onClick={signOut}>
                <ExitToAppIcon />
              </IconButton>
            </Toolbar>
          </Toolbar>
        </AppBar>
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <div className={classes.toolbar} />
          <List>
            <ListItem button component={Link} to={`${match.url}`}>
              <ListItemIcon>
                <DashboardIcon />
              </ListItemIcon>
              <ListItemText primary="Dashboard" />
            </ListItem>
            <ListItem button component={Link} to={`${match.url}/products`}>
              <ListItemIcon>
                <ShoppingCartIcon />
              </ListItemIcon>
              <ListItemText primary="Products" />
            </ListItem>
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Container fixed className={classes.container}>
            <Switch>
              <Route exact path="/admin" component={Dashboard} />
              <Route path="/admin/products" component={Products} />
              <Redirect
                to={{
                  state: { error: true }
                }}
              />
            </Switch>
          </Container>
        </main>
      </div>
    </MuiThemeProvider>
  );
}

export default connect(
  null,
  { signOutAdmin }
)(AdminApp);
