import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../css/ProductItem.css";
import { getOneEntity } from "../redux/actions/adminActions";
import { store } from "../redux/store";

class ProductItem extends Component {
  handleClick = id => {
    store.dispatch(getOneEntity(id));
  };

  render() {
    const { image1Source, image2Source, id, name, price } = this.props;
    return (
      <div className="col-md-3 col-sm-6">
        <div className="product-grid5">
          <div className="product-image5">
            <a href="#">
              <img className="pic-1" src={image1Source} alt="image1Source" />
              <img className="pic-2" src={image2Source} alt="image2Source" />
            </a>

            <ul className="social">
              <li>
                <a href="" data-tip="Add to Wishlist">
                  <i className="fa fa-heart" />
                </a>
              </li>
              <li>
                <a href="" data-tip="Add to Cart">
                  <i className="fa fa-shopping-cart" />
                </a>
              </li>
            </ul>

            <Link to={`/item_details/${id}`} className="select-options">
              <i className="fa fa-arrow-right" />
              More info
            </Link>
          </div>
          <div className="product-content">
            <h3 className="title">
              <a href="#">{name}</a>
            </h3>
            <div className="price">{price}$</div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductItem;
