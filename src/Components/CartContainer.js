import React, { Component } from "react";
import { connect } from "react-redux";
import CartItem from "./CartItem";
import { store } from "../redux/store";
import { getCartEntites } from "../redux/actions/cartActions";

class CartContainer extends Component {
  componentDidMount() {
    store.dispatch(getCartEntites());
  }

  render() {
    let sum = 0;
    const { cartItems } = this.props;
    if (cartItems) {
      cartItems.forEach(element => {
        sum += parseInt(element.price, 10);
      });
    }
    return (
      <div className="container">
        <div className="card shopping-cart">
          {cartItems ? cartItems.map(item => <CartItem {...item} />) : null}
          <div className="card-footer">
            <div className="pull-right" styleName="margin: 10px">
              <button
                type="button"
                className="myadd btn btn-white btn-default pull-right"
              >
                Checkout
              </button>
              <button
                type="button"
                className="btn btn-white btn-default pull-right"
              >
                Total price: <b>{sum}$</b>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  cartItems: state.cart.cartItems
});

export default connect(mapStateToProps)(CartContainer);
