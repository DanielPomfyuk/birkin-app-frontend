import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import "../css/Home.css";

class HomeImage extends Component {
  render() {
    const { link, imageSource } = this.props;
    return (
      <React.Fragment>
        <li>
          <Link to={link}>
            <img src={imageSource} alt="imageSource" />
          </Link>
        </li>
      </React.Fragment>
    );
  }
}

HomeImage.propTypes = {
  link: PropTypes.string.isRequired,
  imageSource: PropTypes.string.isRequired
};

export default HomeImage;
