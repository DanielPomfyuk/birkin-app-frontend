import React, { Component, useEffect } from "react";
import { Switch, Route, withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import App from "./App";
import AdminApp from "./admin/AdminApp";
import NotFound from "./NotFound";
import AdminSignIn from "./admin/AdminSignIn";

const requireAdminSignIn = ReactComponent => {
  return compose(
    withRouter,
    connect(state => ({ authUser: state.reduxTokenAuth.currentUser }))
  )(({ authUser, ...props }) => {
    useEffect(() => {
      if (!authUser.isSignedIn && !authUser.isLoading)
        props.history.push("/admin_sign_in");
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [authUser]);
    return authUser ? <ReactComponent {...props} /> : null;
  });
};

class GlobalErrorSwitch extends Component {
  constructor(props) {
    super(props);
    const { location } = this.props;
    this.previousLocation = location;
  }

  componentWillUpdate(nextProps) {
    const { location } = this.props;

    if (
      nextProps.history.action !== "POP" &&
      (!location.state || !location.state.error)
    ) {
      this.previousLocation = location;
    }
  }

  render() {
    const { location } = this.props;
    const isError = !!(
      location.state &&
      location.state.error &&
      this.previousLocation !== location
    ); // not initial render

    return (
      <div>
        {isError ? (
          <Route component={NotFound} />
        ) : (
          <Switch location={isError ? this.previousLocation : location}>
            <Route path="/admin" component={requireAdminSignIn(AdminApp)} />
            <Route path="/admin_sign_in" component={AdminSignIn} />
            <Route path="/" component={App} />
          </Switch>
        )}
      </div>
    );
  }
}

export default GlobalErrorSwitch;
