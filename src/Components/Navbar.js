import React, { Component } from "react";
import "../css/Navbar.css";

class Navbar extends Component {
  render() {
    return (
      <nav className="navbar-fam">
        <ul>
          <li>
            <a id="logo" href="/">
              {" "}
              <img src="logow.png" alt="logow" />
            </a>
          </li>
          <li>
            <a href="/items_female">Women</a>
          </li>
          <li>
            <a href="/items_male">Men</a>
          </li>
          <li>
            <input
              type="text"
              placeholder="Search for items,brands and inspiration"
              aria-label="Search"
            />
          </li>
          <li id="icon">
            <a href="#" className="fa fa-user" />
          </li>
          <li id="icon">
            <a href="#" className="fa fa-heart navbar-icons" />
          </li>
          <li id="icon">
            <a href="/cart" className="fa fa-shopping-cart navbar-icons" />
          </li>
        </ul>
      </nav>
    );
  }
}
export default Navbar;
