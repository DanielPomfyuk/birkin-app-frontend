import React from "react";
import PropTypes from "prop-types";
import "../css/Footer.css";

function FooterComponent(props) {
  const { footerChildren, icons } = props;
  return (
    <footer className="my_footer_background lalala">
      <div className="container text-center my_footer_background">
        <div className="row my_footer_background">
          {footerChildren.map(child => {
            return (
              <div className="col-md-2 mx-auto my_footer_background">
                <h5 className="font-weight-bold text-uppercase mt-3 mb-4">
                  {child.name}
                </h5>
                <ul className="list-unstyled">
                  {child.links.map(link => {
                    return (
                      <li>
                        <a href="http://google.com">{link}</a>
                      </li>
                    );
                  })}
                </ul>
              </div>
            );
          })}
        </div>
        <ul className="social-network social-circle text-center">
          {icons.map(icon => {
            return (
              <li>
                <a
                  href={icon.href}
                  className={icon.icoClass}
                  title={icon.title}
                >
                  <i className={icon.className} />
                </a>
              </li>
            );
          })}
        </ul>
      </div>

      <div className="footer-copyright text-center py-3 my_footer_background">
        © 2019 Copyright: Pomfyuk | Shafar | Smiriov
      </div>
    </footer>
  );
}

FooterComponent.propTypes = {
  footerChildren: PropTypes.arrayOf.isRequired,
  icons: PropTypes.arrayOf.isRequired
};

export default FooterComponent;
