import React, { Component } from "react";
import PropTypes from "prop-types";
import "../css/CartItem.css";
import { store } from "../redux/store";
import { removeFromCart } from "../redux/actions/cartActions";

class CartItem extends Component {
  handleClick = id => {
    store.dispatch(removeFromCart(id));
  };

  render() {
    const { id, image1Source, name, price } = this.props;
    return (
      <React.Fragment>
        <div className="card-body">
          <div className="row">
            <div className="col-12 col-sm-12 col-md-2 text-center">
              <img
                className="img-responsive"
                src={image1Source}
                alt="prewiew"
                width="150px"
                height="200px"
              />
            </div>
            <div className="col-12 text-sm-center col-sm-12 text-md-left col-md-6">
              <h4 className="product-name">
                <strong>{name}</strong>
              </h4>
              <h4>
                <small>Product description</small>
              </h4>
            </div>
            <div className="col-12 col-sm-12 text-sm-center col-md-4 text-md-right row">
              <div className="col-2 col-sm-3  text-right">
                <h6>
                  <strong>{price} x</strong>
                </h6>
              </div>
              <div className="col-4 col-sm-4 col-md-4">
                <div className="quantity">
                  <input
                    type="number"
                    id="tentacles"
                    name="tentacles"
                    min="1"
                    max="100"
                    className="qty"
                  />
                </div>
              </div>
              <div className="col-2 col-sm-2 col-md-2 text-right">
                <button
                  type="button"
                  className="btn btn-outline-danger btn-xs"
                  onClick={() => this.handleClick(id)}
                >
                  <i className="fa fa-trash" aria-hidden="true" />
                </button>
              </div>
            </div>
          </div>
          <hr />
        </div>
      </React.Fragment>
    );
  }
}

CartItem.propTypes = {
  image1Source: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired
};

export default CartItem;
