export const API_URL = "http://birkin-app-staging.herokuapp.com";
export const ADMIN_AUTH_URL = `${API_URL}/admin_auth`;
export const API_VERSION_1 = `${API_URL}/api/v1`;
