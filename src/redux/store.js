import { createBrowserHistory } from "history";
import { routerMiddleware, connectRouter } from "connected-react-router";
import { applyMiddleware, createStore, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { reduxTokenAuthReducer } from "redux-token-auth";
import cartReducer from "./reducers/cartReducer";
import initialState from "./initial-state";
import adminReducer from "./reducers/adminReducer";

const history = createBrowserHistory();

const rootReducers = combineReducers({
  router: connectRouter(history),
  cart: cartReducer,
  admin: adminReducer,
  reduxTokenAuth: reduxTokenAuthReducer
});

const store = createStore(
  rootReducers,
  initialState,
  composeWithDevTools(
    applyMiddleware(routerMiddleware(history)),
    applyMiddleware(thunk)
  )
);

export { store, history };
