import { generateAuthActions } from "redux-token-auth";
import { ADMIN_AUTH_URL } from "../config";

const config = {
  authUrl: ADMIN_AUTH_URL,
  userAttributes: {
    email: "email"
  },
  userRegistrationAttributes: {
    email: "email"
  }
};

const {
  signInUser: signInAdmin,
  signOutUser: signOutAdmin,
  verifyCredentials: verifyAdminCredentials
} = generateAuthActions(config);

export { signInAdmin, signOutAdmin, verifyAdminCredentials };
