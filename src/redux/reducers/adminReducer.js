import { SET_ALL_ENTITIES, SET_ONE_ENTITY } from "../actions/types";

const initialState = {
  entities: [],
  oneEntity: {},
  attachment_uuids: []
};

const adminReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ALL_ENTITIES:
      return {
        ...state,
        entities: action.payload
      };
    case SET_ONE_ENTITY:
      return {
        ...state,
        oneEntity: action.payload
      };
    default:
      return state;
  }
};

export default adminReducer;
