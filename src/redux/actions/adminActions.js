import axios from "axios";
import { SET_ALL_ENTITIES, SET_ONE_ENTITY } from "./types";
import { API_VERSION_1 } from "../../config";

function fetchAllEntites() {
  return axios.get(`${API_VERSION_1}/sce`);
}

function fetchOneEntity(id) {
  return axios.get(`${API_VERSION_1}/sce/${id}`);
}

function sendPostEntity(sceEntity) {
  return axios.post(
    `${API_VERSION_1}/admin/sce`,
    JSON.stringify({ sce_entity: sceEntity }),
    {
      headers: {
        "Content-Type": "application/json",
        "access-token": localStorage.getItem("access-token"),
        client: localStorage.getItem("client"),
        uid: localStorage.getItem("uid"),
        "token-type": localStorage.getItem("token-type"),
        expiry: localStorage.getItem("expiry")
      }
    }
  );
}

function sendEditEntity(id, sceEntity) {
  return axios.put(
    `${API_VERSION_1}/admin/sce/${id}`,
    JSON.stringify({ sce_entity: sceEntity }),
    {
      headers: {
        "Content-Type": "application/json",
        "access-token": localStorage.getItem("access-token"),
        client: localStorage.getItem("client"),
        uid: localStorage.getItem("uid"),
        "token-type": localStorage.getItem("token-type"),
        expiry: localStorage.getItem("expiry")
      }
    }
  );
}

function sendDeleteEntity(id) {
  return axios.delete(`${API_VERSION_1}/admin/sce/${id}`, {
    headers: {
      "Content-Type": "application/json",
      "access-token": localStorage.getItem("access-token"),
      client: localStorage.getItem("client"),
      uid: localStorage.getItem("uid"),
      "token-type": localStorage.getItem("token-type"),
      expiry: localStorage.getItem("expiry")
    }
  });
}

function setAllEntities(entities) {
  return {
    type: SET_ALL_ENTITIES,
    payload: entities
  };
}

function setOneEntities(oneEntity) {
  return {
    type: SET_ONE_ENTITY,
    payload: oneEntity
  };
}

export function getAllEntites() {
  return dispatch => {
    return fetchAllEntites().then(response =>
      dispatch(setAllEntities(response.data.entities))
    );
  };
}

export function getOneEntity(id) {
  return dispatch => {
    return fetchOneEntity(id).then(response =>
      dispatch(setOneEntities(response.data.entity))
    );
  };
}

export function addEntity(sceEntity) {
  return dispatch => {
    return sendPostEntity(sceEntity).then(() => dispatch(getAllEntites()));
  };
}

export function editEntity(id, sceEntity) {
  return dispatch => {
    return sendEditEntity(id, sceEntity).then(() => dispatch(getAllEntites()));
  };
}

export function deleteEntity(id) {
  return dispatch => {
    return sendDeleteEntity(id).then(() => dispatch(getAllEntites()));
  };
}
