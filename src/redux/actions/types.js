export const FETCH_CART_ITEMS = "FETCH_CART_ITEMS";

// Admin
export const SET_ALL_ENTITIES = "SET_ALL_ENTITIES";
export const SET_ONE_ENTITY = "SET_ONE_ENTITY";
