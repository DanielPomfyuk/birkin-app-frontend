import axios from "axios";
import { FETCH_CART_ITEMS } from "./types";
import { API_VERSION_1 } from "../../config";

function fetchCartEntites() {
  return axios.get(`${API_VERSION_1}/sce/filter/from_cart`);
}

function setCartEntities(entities) {
  return {
    type: FETCH_CART_ITEMS,
    payload: entities
  };
}

function sendAddToCart(id) {
  return axios.post(
    `${API_VERSION_1}/sce/add_to_cart/${id}`,
    JSON.stringify({}),
    {
      headers: {
        "Content-Type": "application/json"
      }
    }
  );
}

function sendRemoveFromCart(id) {
  return axios.post(
    `${API_VERSION_1}/sce/delete_from_cart/${id}`,
    JSON.stringify({}),
    {
      headers: {
        "Content-Type": "application/json"
      }
    }
  );
}

export function getCartEntites() {
  return dispatch => {
    return fetchCartEntites().then(response =>
      dispatch(setCartEntities(response.data.entities))
    );
  };
}

export function addToCart(id) {
  return dispatch => {
    return sendAddToCart(id).then(() => dispatch(getCartEntites()));
  };
}

export function removeFromCart(id) {
  return dispatch => {
    return sendRemoveFromCart(id).then(() => dispatch(getCartEntites()));
  };
}
